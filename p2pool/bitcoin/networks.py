import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

def get_subsidy(height):
    if height <= 8000:
        return 0
    halvings = height // 440000
    return (10 * 100000000) >> halvings

nets = dict(
    checkcoin=math.Object(
        P2P_PREFIX='bf0c6bbd'.decode('hex'),
        P2P_PORT=11070,
        ADDRESS_VERSION=28,
        RPC_PORT=21070,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'darkcoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda nBits, height: get_subsidy(height+1),
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        BLOCK_PERIOD=60, # s
        SYMBOL='CKC',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Checkcoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Checkcoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.checkcoin'), 'checkcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://cryptobe.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://cryptobe.com/address/',
        TX_EXPLORER_URL_PREFIX='http://cryptobe.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**22 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
